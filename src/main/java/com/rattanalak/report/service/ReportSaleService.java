/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rattanalak.report.service;


import com.rattanalak.report.dao.ReportSaleDao;
import com.rattanalak.report.model.ReportSale;
import java.util.List;

/**
 *
 * @author Rattanalak
 */
public class ReportSaleService {
 public List<ReportSale> getReportSaleByDay() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport(year);
    }

}


