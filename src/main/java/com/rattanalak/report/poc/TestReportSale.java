/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rattanalak.report.poc;

import com.rattanalak.report.model.ReportSale;
import com.rattanalak.report.service.ReportSaleService;
import java.util.List;

/**
 *
 * @author Rattanalak
 */
public class TestReportSale {
    public static void main(String[] args) {
        ReportSaleService reportsaleService = new ReportSaleService();

//        List<ReportSale> report = reportsaleService.getReportSaleByDay();
//        for (ReportSale r : report) {
//            System.out.println(r);
//        }

        List<ReportSale> reportMonth = reportsaleService.getReportSaleByMonth(2013);
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }
    }

}
